﻿using APICarPooling.Core.Model;

namespace APICarPooling.Core.Interfaces
{
	public interface ICarRepository
    {
        Task AddAvailableCarsAsync(List<CarAvailable> cars);
        Task RemoveAllPreviousCarsAsync();
        Task<CarAvailable> ReturnTheCarOfTheGroupAsync(int groupID);
        Task<List<CarAvailable>> FindAvailableCarsAsync();
    }

}

