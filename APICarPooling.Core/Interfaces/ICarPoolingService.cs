﻿using System;
using APICarPooling.Core.Model;
using System.Runtime.InteropServices;

namespace APICarPooling.Core.Interfaces
{
	public interface ICarPoolingService
	{

        Task AddAvailableCarsService(List<CarAvailable> cars);
        Task PerformJourneyService(List<Traveler> people);
        Task<int> RequestToBeDroppedOffService(int groupID);
        Task<CarAvailable> ReturnTheCarOfTheGroupService(int groupID);
        Task<Journey> AssignCarToWaitingGroupService(int groupID);
       
    }
}

