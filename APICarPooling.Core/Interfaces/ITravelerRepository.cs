﻿
using System;
using APICarPooling.Core.Model;

namespace APICarPooling.Core.Interfaces
{
	public interface ITravelerRepository
	{
        Task<Traveler> SelectTravelerAsync(int groupID);
        Task RequestAJourneyAsync(List<Traveler> traveler);
        Task RequestToBeDroppedOffAsync(Traveler traveler);
    }
}

