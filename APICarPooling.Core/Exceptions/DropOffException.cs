﻿using System;
namespace APICarPooling.Core.Infrastructure
{
	public sealed class DropOffException : Exception 
	{
		public DropOffException()
		{
		}

        public DropOffException(string message) : base(message)
        {
        }
    }
}

