﻿using System;
namespace APICarPooling.Core.Exceptions
{
	public sealed class LocateException : Exception
	{
		public LocateException()
		{
		}

        public LocateException(string message) : base(message)
        {
        }
    }
}

