﻿using System;
namespace APICarPooling.Core.Infrastructure
{
	public sealed class CarException : Exception
	{
        public CarException()
        {
        }
        public CarException(string message) : base(message)
		{
		}
	}
}

