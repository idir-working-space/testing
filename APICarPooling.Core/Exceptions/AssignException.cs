﻿using System;
namespace APICarPooling.Core.Exceptions
{
	public sealed class AssignException : Exception
	{
		public AssignException()
		{
		}

		public AssignException(string message) : base(message)
		{

		}
	}
}

