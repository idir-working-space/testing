﻿using System;
using APICarPooling.Controllers;
using APICarPooling.Core.Interfaces;
using APICarPooling.Core.Model;
using APICarPooling.Logging.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace APICarPooling.Test
{
	public class TravelerTest
	{

        [Fact]
        public async Task GroupPeoplePerformAJourney_ReturnsOkResult_WhenGroupRegisteredSuccessfully()
        {
            // Arrange
            var mockCarPoolingService = new Mock<ICarPoolingService>();

            var controller = new CarPoolingController(
               mockCarPoolingService.Object);

            var traveler = new List<Traveler>
            {
                new Traveler { Id = 1, People = 4 },
                new Traveler { Id = 2, People = 6 }
            };

            // Act
            var result = await controller.PerformJourney(traveler);

            // Assert
            Assert.IsType<OkObjectResult>(result.Result);
            var okResult = result.Result as OkObjectResult;
            Assert.Equal(traveler, okResult.Value);
        }
        [Fact]
        public async Task GroupPeoplePerformAJourney_ThrowsException_WhenCarsNotAvailable()
        {
            // Arrange

            var groupOfPeopleToLoadMock = new List<Traveler>();

            var mockCarPoolingService = new Mock<ICarPoolingService>();

            var controller = new CarPoolingController(
               mockCarPoolingService.Object);

            mockCarPoolingService.Setup(repo => repo.PerformJourneyService(groupOfPeopleToLoadMock)).Throws(new Exception("Test exception. Groups of people's count == 0"));
           
            var groupOfPeople = new List<Traveler>();
            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => controller.PerformJourney(groupOfPeople));
        }
    }
}

