﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APICarPooling.Logging.Model
{
	public class Log
	{
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string MessageError { get; set; }
        public string StackTrace { get; set; }
        public DateTime RegisterDate { get; set; }
    }
}

