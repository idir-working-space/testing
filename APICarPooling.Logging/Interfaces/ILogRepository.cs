﻿using System;
namespace APICarPooling.Logging.Interfaces
{
	public interface ILogRepository
	{
		Task RegisterErrorAsync(string Message, string StackTrace);
	}
}

