﻿using APICarPooling.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using APICarPooling.Persistence.Data;
using APICarPooling.Logging.Interfaces;

namespace APICarPooling.Persistence
{
    public static class ServiceCollectionExtensions
    {
        public static void AddApplicationDbContextAndRepositories(this IServiceCollection serviceCollection, string connectionString)
        {
            serviceCollection.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(connectionString));
            serviceCollection.AddScoped<ICarRepository, CarRepository>();
            serviceCollection.AddScoped<ITravelerRepository, TravelerRepository>();
            serviceCollection.AddScoped<IJourneyRepository, JourneyRepository>();
            serviceCollection.AddScoped<ILogRepository, LogRepository>();
            serviceCollection.AddScoped<ICarPoolingService, CarPoolingServiceRepository>();

        }
    }
}
