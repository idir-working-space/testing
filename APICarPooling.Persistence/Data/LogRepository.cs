﻿using System;
using APICarPooling.Logging.Interfaces;
using APICarPooling.Logging.Model;
using Microsoft.EntityFrameworkCore;

namespace APICarPooling.Persistence.Data
{
    public class LogRepository: ILogRepository
    {
        private readonly ApplicationDbContext _carPoolingDbcontext;

        public LogRepository(ApplicationDbContext carPoolingContext)
        {
            _carPoolingDbcontext = carPoolingContext;
        }

        public async Task RegisterErrorAsync(string Message, string StackTrace)
        {
            var log = MapValues(Message,StackTrace);

            _carPoolingDbcontext.Log.Add(log);
           
            await _carPoolingDbcontext.SaveChangesAsync();
        }

        private Log MapValues(string Message, string StackTrace)
        {

            Log log = new Log();
            log.MessageError = Message;
            log.StackTrace = StackTrace;
            log.RegisterDate = DateTime.UtcNow;

            return log;
        }

    }
}

