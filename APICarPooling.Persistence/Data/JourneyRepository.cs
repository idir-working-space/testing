﻿using System;
using APICarPooling.Core.Interfaces;
using APICarPooling.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace APICarPooling.Persistence.Data
{
	public class JourneyRepository : IJourneyRepository
	{
        private readonly ApplicationDbContext _carPoolingDbcontext;

        public JourneyRepository(ApplicationDbContext carPoolingContext)
        {
            _carPoolingDbcontext = carPoolingContext;
        }

        public async Task RequestToBeDroppedOffAsync(Journey journey)
        {
            _carPoolingDbcontext.Journey.Remove(journey);
            await _carPoolingDbcontext.SaveChangesAsync();
        }

        public async Task<Journey> SelectJourneyByTravelerAsync(int groupID)
        {
            return await _carPoolingDbcontext.Journey.FirstOrDefaultAsync(p => p.Id == groupID);
        }

        public async Task<Journey> AssignCarToWaitingGroupToPerformAJourneyAsync(Traveler traveler, List<CarAvailable> carAvailables)
        {
            //donde los asientos correspondan.
            Journey journey = new Journey();

            foreach(var car in carAvailables)
            {
                if(car.Seats >= traveler.People)
                {
                    journey.CarId = car.Id;
                    journey.TravelerId = traveler.Id;
                }
                
            }

            _carPoolingDbcontext.Journey.Add(journey);
            await _carPoolingDbcontext.SaveChangesAsync();

            return journey;

        }
    }
}

