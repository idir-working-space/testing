﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using APICarPooling.Core.Model;
using APICarPooling.Logging.Model;

namespace APICarPooling.Persistence.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public class CatalogContextDesignFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
        {
            public ApplicationDbContext CreateDbContext(string[] args)
            {
                var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlite("Data Source=CarPooling.db");

                return new ApplicationDbContext(optionsBuilder.Options);
            }
        }

        public DbSet<CarAvailable> CarAvailable { get; set; }
        public DbSet<Traveler> Traveler { get; set; }
        public DbSet<Journey> Journey { get; set; }
        public DbSet<Log> Log { get; set; }

    }
}