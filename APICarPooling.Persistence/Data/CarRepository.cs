﻿using APICarPooling.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using APICarPooling.Core.Model;
using System.Text.RegularExpressions;

namespace APICarPooling.Persistence.Data
{
    public class CarRepository : ICarRepository
    {
        private readonly ApplicationDbContext _carPoolingDbcontext;

        public CarRepository(ApplicationDbContext carPoolingContext)
        {
            _carPoolingDbcontext = carPoolingContext;
        }
        public async Task AddAvailableCarsAsync(List<CarAvailable> cars)
        {
           
            foreach (var car in cars)
            {
                if(car.Seats > 0)
                _carPoolingDbcontext.CarAvailable.Add(car);   
            }
            await _carPoolingDbcontext.SaveChangesAsync();
        }

        public async Task RemoveAllPreviousCarsAsync()
        {
            await _carPoolingDbcontext.CarAvailable.ExecuteDeleteAsync();
            await _carPoolingDbcontext.SaveChangesAsync();

        }

        public async Task<CarAvailable> ReturnTheCarOfTheGroupAsync(int groupID)
        {
            var journeyAssigned = await _carPoolingDbcontext.Journey.FirstOrDefaultAsync(p => p.TravelerId == groupID);

            if(journeyAssigned is not null)
                return await _carPoolingDbcontext.CarAvailable.FirstOrDefaultAsync(p => p.Id == journeyAssigned.CarId);

            return null;
        }

        public async Task<List<CarAvailable>> FindAvailableCarsAsync()
        {
            return await _carPoolingDbcontext.CarAvailable.ToListAsync();
        }


    }
}
