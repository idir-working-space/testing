FROM mcr.microsoft.com/dotnet/sdk:6.0 as build
WORKDIR /car
EXPOSE 9091

# copy NuGet config
#COPY NuGet.config .

#Creación de carpetas
RUN mkdir -p /car/APICarPooling
RUN mkdir -p /car/APICarPooling.Core
RUN mkdir -p /car/APICarPooling.Logging
RUN mkdir -p /car/APICarPooling.Persistence
RUN mkdir -p /car/APICarPooling.Test


#copiado de los archivos
COPY *.sln .
COPY ./APICarPooling/*.csproj ./APICarPooling
COPY ./APICarPooling.Core/*.csproj ./APICarPooling.Core
COPY ./APICarPooling.Logging/*.csproj ./APICarPooling.Logging
COPY ./APICarPooling.Persistence/*.csproj ./APICarPooling.Persistence
COPY ./APICarPooling.Test/*.csproj ./APICarPooling.Test

#RUN dotnet restore 

COPY  . .
WORKDIR /car
RUN dotnet publish -c Release -o out


FROM mcr.microsoft.com/dotnet/sdk:6.0
WORKDIR /car
COPY --from=build /car/out .
EXPOSE 9091
ENV ASPNETCORE_URLS=http://+:9091
ENTRYPOINT ["dotnet","APICarPooling.dll"]
# dotnet run --launch-profile nombre-perfil
# ASPNETCORE_URLS=http://+:8080
