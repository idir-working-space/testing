using APICarPooling.Persistence;
using APICarPooling.Persistence.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using HealthChecks.UI.Client;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddApplicationDbContextAndRepositories(builder.Configuration.GetConnectionString("CarPoolingConnection"));
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHealthChecks();
builder.WebHost.UseUrls("http://0.0.0.0:9091");

var app = builder.Build();

// Configure the HTTP request pipeline.
  app.UseSwagger();
    app.UseSwaggerUI();
    app.MapHealthChecks("/status", new HealthCheckOptions
    {
        Predicate = _ => true,
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse

    });



app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
