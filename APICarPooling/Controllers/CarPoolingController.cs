﻿using Microsoft.AspNetCore.Mvc;
using APICarPooling.Core.Model;
using APICarPooling.Core.Interfaces;
using APICarPooling.Persistence.Data;
using APICarPooling.Logging.Interfaces;
using System.Text.RegularExpressions;

namespace APICarPooling.Controllers;


[Route("APICarPooling")]
[ApiController]
public class CarPoolingController : ControllerBase
{

    private readonly ICarPoolingService _carPoolingServiceRepository;


    public CarPoolingController(ICarPoolingService carPoolingServiceRepository)
    {
        this._carPoolingServiceRepository = carPoolingServiceRepository;
       
    }

    /// <summary>
    /// Load the list of available cars in the service and remove all previous data (reset the application state).
    /// Required The list of cars to load.
    /// </summary>
    /// <param name="cars">required The list of cars to load.</param>
    /// <returns>When the list is registered correctly.</returns>
    /// <exception cref="Exception"></exception>
    [HttpPut]
    [Route("/cars")]
    [ProducesResponseType(typeof(CarAvailable), 200)]
    [ProducesResponseType(typeof(ProblemDetails), 400)]
    public async Task<ActionResult<CarAvailable>> AddAvailableCars(List<CarAvailable> cars)
    {

        await _carPoolingServiceRepository.AddAvailableCarsService(cars);

        return Ok(cars);
        
    }

    /// <summary>
    /// A group of people requests to perform a journey.
    /// </summary>
    /// <param name="people">required The group of people that wants to perform the journey</param>
    /// <returns>required The group of people that wants to perform the journey</returns>
    /// <exception cref="Exception"></exception>
    [HttpPost]
    [Route("/journey")]
    [ProducesResponseType(typeof(CarAvailable), 200)]
    [ProducesResponseType(typeof(CarAvailable), 202)]
    [ProducesResponseType(typeof(ProblemDetails), 400)]
    public async Task<ActionResult<Traveler>> PerformJourney(List<Traveler> people)
    {
        await _carPoolingServiceRepository.PerformJourneyService(people);

        return Ok(people);
    }

    /// <summary>
    /// A group of people requests to be dropped off. Whether they traveled or not.
    /// </summary>
    /// <param name="groupID">required A form with the group ID, such that ID=X</param>
    /// <returns>When the group is unregistered correctly. 0 NoFound(), 1 NoContent()</returns>
    [HttpPost]
    [Route("/dropoff")]
    [ProducesResponseType(typeof(int), 200)]
    [ProducesResponseType(typeof(int), 204)]
    [ProducesResponseType(typeof(ProblemDetails), 400)]
    [ProducesResponseType(typeof(ProblemDetails), 404)]
    public async Task<ActionResult<int>> RequestToBeDroppedOff(int groupID)
    {
        var response = await _carPoolingServiceRepository.RequestToBeDroppedOffService(groupID);

        return response == 0 ? NotFound() : NoContent(); 
    }

    /// <summary>
    /// Given a group ID such that ID=X, return the car the group is traveling with, or no car if they are still waiting to be served.
    /// </summary>
    /// <param name="groupID">required A url encoded form with the group ID such that ID=X</param>
    /// <returns>With the car as the payload when the group is assigned to a car.</returns>
    /// <exception cref="Exception"></exception>
    [HttpPost]
    [Route("/locate")]
    [ProducesResponseType(typeof(CarAvailable), 200)]
    [ProducesResponseType(typeof(int), 204)]
    [ProducesResponseType(typeof(ProblemDetails), 400)]
    [ProducesResponseType(typeof(ProblemDetails), 404)]
    public async Task<ActionResult<CarAvailable>> ReturnTheCarOfTheGroup(int groupID)
    {
        var response = await _carPoolingServiceRepository.ReturnTheCarOfTheGroupService(groupID);

        return response == null ? NotFound() : Ok(response);
    }

    /// <summary>
    /// Addition function to assign a car to a group that they want to travel or they are waiting to travel
    /// </summary>
    /// <param name="groupID">required A url encoded form with the group ID such that ID=X</param>
    /// <returns>With the car as the payload when the group is assigned to a car.</returns>
    /// <exception cref="Exception"></exception>
    [HttpPost]
    [Route("/assign")]
    [ProducesResponseType(typeof(int), 200)]
    [ProducesResponseType(typeof(ProblemDetails), 400)]
    [ProducesResponseType(typeof(ProblemDetails), 404)]
    public async Task<ActionResult<Journey>> AssignCarToWaitingGroup(int groupID)
    {
        var response = await _carPoolingServiceRepository.AssignCarToWaitingGroupService(groupID);

        return response == null ? NotFound() : Ok(response);
    }
}

